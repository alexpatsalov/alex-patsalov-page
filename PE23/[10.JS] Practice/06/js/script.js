function buildTower (numberOfFloors){
	const numberOfBricks = 2 * numberOfFloors - 1;
	const result = [];
	for (let i = 0; i < numberOfFloors; i++) {
		result.push(buildFloor(i + 1, numberOfBricks));
	}
	return result;
}

function buildFloor (currentFloor, numberOfBricks){
	let result = '';
	let stars = (2 * currentFloor) - 1;
	let spaces = (numberOfBricks - stars)/2;
	return ' '.repeat(spaces) + '*'.repeat(stars) + ' '.repeat(spaces);
}
