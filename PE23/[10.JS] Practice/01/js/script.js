// An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines whether a string that contains only letters is an isogram. Assume the empty string is an isogram. Ignore letter case.

function onCheckBtnClick() {
	const resElement = document.getElementById('result');
	const inputElement = document.getElementById('word');
	let result = inputElement && isIsogram(inputElement.value);

	if (resElement) {

		resElement.innerHTML = result ? 'Ізограм' : 'Не ізограм';
	}
}

function isIsogram(str) {
	const lowerCase = str.toLowerCase();
	const arr = lowerCase.split('');

	for (let i = 0; i < arr.length; i++){
		if (lowerCase.indexOf(arr[i], i + 1) >= 0){
			return false;
		}
	}
	return true;
}

let string = 'raAFafFsfGddg';
isIsogram (string);