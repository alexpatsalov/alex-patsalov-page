function getRandomImage (){
	const imgArr = ['./images/1.jpg', './images/2.jpg', './images/3.jpg', './images/4.jpg', './images/5.jpg', './images/6.jpg', './images/7.png', './images/8.jpeg', './images/9.jpg', './images/10.jpg' ];
	return imgArr[Math.floor(Math.random() * imgArr.length)]
}
const container = document.getElementById('container');
function shuffle (container, numOfShuffle = 6){
	container.innerHTML = '';
	for (let i = 0; i < numOfShuffle; i++) {
		const img = document.createElement('img');
		img.setAttribute('src', getRandomImage());
		container.append(img);
	}
}

container.onclick = function(){
	shuffle(container);
};
shuffle(container);




