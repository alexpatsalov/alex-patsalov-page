// Создать объект student, у которого будут такие свойства: имя (name), фамилия (last name), коэффициент лени (laziness) равный 4 и коэффициент хитрости (trick) равный 5. Вывести его в консоль.
//
// Если коэффициент лени больше или равен 3, и при этом меньше или равен 5, а коэффициент хитрости меньше или равен 4 - вывести в консоль сообщение "Cтудент имяСтудента фамилияСтудента отправлен на пересдачу".

// Скопируйте объект student из задачи 1, после чего напишите такой код:
//
//     измените коэффициент лени на 6, а коэффициент хитрости - на 3;
// если коэффициент лени больше или равен 5, а коэффициент хитрости меньше 4, вывести в консоль сообщение "Cтудент имяСтудента фамилияСтудента передан в военкомат".
//
// let student = {
//     name : 'Mike',
//     last_name : 'McMaster',
//     laziness : 4,
//     trick : 5
// }
// console.log(student);
//
// if (student.laziness >= 3 && student.laziness <= 5 && student.trick <= 4){
//     console.log ('Студент ' + student.name + ' ' + student.last_name + ' отправлен на пересдачу' )
// }
//
// student.laziness = 6;
// student.trick = 3;
//
// if (student.laziness >= 5 && student.trick <= 4){
//     console.log (`Студент ${student.name}${student.last_name} передан в военкомат`)
// }

// Cоздать объект product c такими полями: name, full name, price, availability (есть ли на складе) со значение false, и additional gift (подарок к покупке) со значением null. Вывести его в консоль.
// let product = {
//     name : undefined,
//     "full name" : undefined,
//     price : undefined,
//     availability : false,
//     "additional gift" : null
// }
//
// console.log(product);

// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Спросить у пользователя "Что вы хотите узнать о студенте?" и вывести в консоль эту информацию.

// const danItStudent = {
//     name : 'Alex',
//     lastname : 'Vasia',
//     HW : 12
// }
//
// const question = prompt ('What do you need?');
// console.log(danItStudent[question]);

// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Вывести объект в консоль. После чего спрашивать у пользователя в цикле два вопроса - "Какое свойство вы хотите изменить?", потом - "На какое значение?". Цикл может иметь максимум 3 итерации. Если пользователь нажмет Cancel на любой из вопросов - досрочно прервать цикл. Вывести объект в консоль.

// const danItStudent = {
//     name : 'Alex',
//     lastname : 'Vasia',
//     HW : 12
// }
//
// console.log(danItStudent);
//
// for (let i = 0; i < 2; i++){
//     let question1 = prompt ('Что изменить?');
//     let question2 = prompt ('На какое значение?');
//     if (question1 === null || question2 === null){
//         break;
//     }
//     danItStudent["question1"] = "question2";
// }
//
// console.log(danItStudent);

// 	Создайте массив styles с элементами «Джаз» и «Блюз».
// Добавьте «Рок-н-ролл» в конец.
// 	Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
// 	Удалите первый элемент массива и покажите его.
// 	Вставьте «Рэп» и «Регги» в начало массива.
//
// 	Массив по ходу выполнения операций:
//
// 	Джаз, Блюз
// Джаз, Блюз, Рок-н-ролл
// Джаз, Классика, Рок-н-ролл
// Классика, Рок-н-ролл
// Рэп, Регги, Классика, Рок-н-ролл

// let styles = ['джаз', 'блюз'];
// console.log(styles);
// styles.push('рок-н-ролл');
// console.log(styles);
// styles[1] = "класика";
// console.log(styles);
// console.log(styles.shift());
// styles.unshift("рэп");
// styles.unshift("регги");
// console.log(styles);

// Напишите функцию sumInput(), которая:
//
// Просит пользователя ввести значения, используя prompt и сохраняет их в массив.
// 	Заканчивает запрашивать значения, когда пользователь введёт не числовое значение, пустую строку или нажмёт «Отмена».
// Подсчитывает и возвращает сумму элементов массива.
// 	P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0».
//
// function sumInput() {
// 	let arr = [];
// 	while (true) {
// 		let number = prompt('Insert number');
// 		if (number === null || number === '' || !isFinite(number)) break;
// 		arr.push(+number);
// 	}
// 	let sum = 0;
// 	for (let number of arr){
// 		sum += number;
// 	}
// 	return sum;
// }
//
// console.log('res -------->>>', sumInput());