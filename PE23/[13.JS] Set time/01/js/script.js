document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
	if (localStorage.getItem('name') === null){
		let firstName = prompt('What is your name?');
		localStorage.setItem('name', firstName);
	} else {
		alert (`Hi ${localStorage.getItem('name')}`);
	}
}