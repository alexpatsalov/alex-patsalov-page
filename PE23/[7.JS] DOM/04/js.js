const list = document.getElementsByTagName('li');
const styles = {
	backgroundColor: '#fafafa',
};
for (const style in styles) {
	console.log(style, styles);
}
for (const el of list){
	el.style.color = `rgb(${Math.ceil(Math.random()*255)}, ${Math.ceil(Math.random()*255)}, ${Math.ceil(Math.random()*255)})`;

	applyStyles(el, styles);

}

function applyStyles (element, styles){
	for (const style in styles) {
		element.style[style] = styles[style];
	}
}

