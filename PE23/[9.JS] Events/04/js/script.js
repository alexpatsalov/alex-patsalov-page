function onBuildBtnClick(){
	let array = [
		['Alex', 'Anton','Sergey'],
		['Alex', 'Anton','Sergey'],
		['Alex', 'Anton','Sergey']
	];
	const rootEl = document.getElementById('Menu');

	if (rootEl === null){
		console.error('No element');
		return;
	}
	for (let i = 0; i < array.length; i++){
		const rootLi = document.createElement('li');
		const subUl = document.createElement('ul');
		const button = document.createElement('button');
		button.innerHTML = 'Button me!!';
		button.onclick = function(){
			rootLi.classList.toggle('hidden');
		}


		rootLi.innerHTML = Math.random() * 255;
		for (let j = 0; j < array[i].length; j++){
			const subLi = document.createElement('li');
			subLi.innerHTML = array[i][j];
			subUl.append(subLi);
		}
		rootLi.append(button);
		rootLi.append(subUl);
		rootEl.append(rootLi);
	}
}



