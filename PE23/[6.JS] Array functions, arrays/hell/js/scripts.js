function getAllProducts(products) {
	return products.sort((p1,p2) => p1.title - p2.title);
}

function getPopularProducts(products) {
	return products.sort((p1, p2) => p1.popularIndex - p2.popularIndex);
}

function getViewedProducts(products) {
	return products.sort((p1, p2) => p1.viewed - p2.viewed);
}


/**
 * @return {String}
 **/
function buildProductItem(product) {
	return '';
}
