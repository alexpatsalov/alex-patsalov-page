const obj1 = {title: 'literal obj'};

const obj2 = Object.create(Object.prototype);
obj2.title = 'Second obj';

function Person(firstName) {
	this.firstName = firstName;

}

Person.prototype.log = function (message) {
	return console.log(message)
}

const obj3 = new Person('Max'); /*экземпляр класса (фукции-констурктора) Person*/
// obj3.method('hello');

const obj31 = new Person ('Serhii');
// obj31.method('hi');