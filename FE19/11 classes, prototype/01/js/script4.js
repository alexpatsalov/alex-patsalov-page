class Warrior {
	constructor(health = 1000, power = 100) {
		this.health = health;
		this.power = power;
		this._points = 0;
	};
	addPoints (points){
		this._points += points;
		return this;
	};
	set Setter(x) {
		this.x = x;

	}
	get Getter() {
		return x;
	}
}
class Human extends Warrior{
	constructor() {
		super(800, 100);
	}
}
class Ork extends Warrior{
	constructor() {
		super(2000, 150);
	}
}
class Elf extends Warrior{
	constructor(health, power) {
		super(health, power);
	}
}
class WhiteElf extends Elf{
	constructor() {
		super(500, 80);
	}
}
class BlackElf extends Elf{
	constructor() {
		super(600, 90);
	}
}