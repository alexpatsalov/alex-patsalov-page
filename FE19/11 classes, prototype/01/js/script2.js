function Person(firstName, lastName) {
	this.firstName = firstName;
	this.lastName = lastName;
}

Person.prototype.getFullName = function () {
	return `${this.firstName} ${this.lastName}`;
}

function User(email, password, firstName, lastName) {
	this.email = email;
	this.password = password;
	Person.call(this, firstName, lastName);
}

User.prototype = Object.create(Person.prototype);
User.prototype.getFullName = function () {
	return `${Person.prototype.getFullName.call(this)} !!`
}
User.prototype.authenticate = function () {
	return console.log('Authenticate');
}

const person = new Person ('Alex', 'Patsalov');
const user = new User ('aaaa@gmail.com', '212121','Max', 'bilyk');