document.addEventListener('DOMContentLoaded', function () {

	let ulTag = document.createElement('ul');
	ulTag.setAttribute('id', 'ul-wrapper');

	document.body.appendChild(ulTag);
	let liRequest = ulTag.innerHTML = '<li>Request sent...</li>';
	ulTag.onclick = onPersonBntClick;

	Ajax.getRequest('https://swapi.dev/api/people')
		.then(function (response) {
			ulTag.innerHTML = response.results
				.map((item, index) => `<li>${new Person(item)}<button>DELETE</button></li>`)
				.join('');



			console.log('Response --->>', response);
		});
});

function onPersonBntClick(event) {
	// const ul = document.getElementById(parent);
	// console.log(ul.children.item(index));
	// ul.removeChild(ul.children.item(index));
	const element = event.target;
	if (element.tagName === 'BUTTON'){
		(element.closest('li')).remove();
	}

}