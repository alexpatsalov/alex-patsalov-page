document.addEventListener('DOMContentLoaded', function() {
	const button = document.getElementById('testConnection');

	button.onclick = onSendRequestBtnClick;
});

/**
 * @desc Execute HTTP request
 **/
function onSendRequestBtnClick() {
	const xhttp = new XMLHttpRequest();
	xhttp.open('GET', 'https://swapi.dev/api/people', true);

	xhttp.onreadystatechange = function (){

		try {
			if (xhttp.readyState === 4 && xhttp.status === 200) {
				console.log(' --->', JSON.parse(xhttp.responseText));
			}
		} catch (e) {
			console.log('Error -> ', e);
		}

	};

	xhttp.send();

}

