const user = {
	name: 'Max',
	age: 14,
	address: {
		city: 'Kyiv',
		country: 'Ukraine',
		zip: '02152'
	}
}

try{

	localStorage.setItem('user', JSON.stringify(user));

	let str = localStorage.getItem('user');
	let parsedUser = JSON.parse(str);
	console.log(parsedUser);

	const string = '{"user": Alex}'//intentional error
	console.log(JSON.parse(string)); //intentional error
}
catch (e) {

	console.log(e);

}
