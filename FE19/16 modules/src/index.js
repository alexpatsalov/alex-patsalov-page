import {toISOFormat} from "./utils/dateAndTime";
import {PlanetService} from "./services/PlanetService";

import {Planet} from "./models/Planet";

PlanetService
	.getPlanets()
	.then(({data}) => {
		// console.log(data.results.map(planet => new Planet (planet)));
		document.write(data.results.map(planet => `Planet -->> ${new Planet(planet)}`
		).join(''))
	});

console.log(toISOFormat(Date.now()));