/**
 * @desc Дата в формате ISO
 * @param {String} str - date
 * @return {string}
 * */

export const toISOFormat = (str) => {
	return new Date(str).toISOString();
}

