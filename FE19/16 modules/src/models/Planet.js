export class Planet {
	constructor(props) {
		const {
			climate,
			created,
			edited,
			gravity,
			name
		} = props;

		this.climate = climate;
		this.created = created;
		this.edited = edited;
		this.gravity = gravity;
		this.name = name;

	}
	toString(){
		return `<ul><li><strong>climate:</strong>${this.climate}</li></ul>`
	}
}