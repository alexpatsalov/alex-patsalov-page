import axios from 'axios';

export const max = 5000;

export default class BaseService{
	constructor() {
	}

	static async requestGet (url){
		return axios.get(url);
	}
}