import Base from './BaseService';
import {max} from "./BaseService";

export class PlanetService extends Base{
	constructor() {
		super();
	}

	static async getPlanets (){
		return PlanetService.requestGet('http://swapi.dev/api/planets/')
	}
}