const gulp = require('gulp');
const uglify = require('gulp-uglify-es');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
	return gulp.src('src/scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('dist/css'));
});

gulp.task('script', function () {
	return gulp
		.src('src/js/script.js')
		.pipe(uglify())
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest('dist/js'));
});

// gulp.task('magic', function () {
// 	return gulp
// 		.src(
// 				'./node_modules/bootstrap/dist/css/bootstrap.css',
// 		)
// 		.pipe(concat('vendor.css'))
// 		.pipe(gulp.dest('dist/css'));
// });
gulp.task('htmlmin', () => {
	return gulp.src('src/index.html')
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {
	return gulp
		.watch(
			'src/**/*.*',
			gulp.series(
				'htmlmin',
				'sass',
				'script'
			)
		)
});
