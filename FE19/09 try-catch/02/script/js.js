document.addEventListener('DOMContentLoaded', function () {
	let form = document.getElementById('form');
	form.onsubmit = onSubmit;
})

function onSubmit(e) {
	e.preventDefault();
	const {target:form} = e;
	let data = {};

	for (let el of form){
		data[el.name] = el.value;
	}

	try {
		let person = new Worker(data);
		console.log(person.getFullName());
	} catch (e) {
		let errorBlock = document.getElementById('error-container');
		errorBlock.innerHTML = e.message;
		errorBlock.classList.toggle('active');
	}
}

function Worker({name, surname, position, weight, height}) {
	if (!name) {
		throw new Error('Name is required');
	}
	if (!surname) {
		throw new Error('Surname is required');
	}
	this.name = name;
	this.surname = surname;
	this.position = position;
	this.weight = weight;
	this.height = height;
}

Worker.prototype.getFullName = function () {
	return `${this.name}${this.surname}`
}

