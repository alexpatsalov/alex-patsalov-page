const destinationPrices = [
	{dest: 'Pechersk', index: 5.5},
	{dest: 'Obolon', index: 2.5},
	{dest: 'Troeschyna', index: 0.5},
];

const materialPrices = [
	{title: 'brick', index: 10},
	{title: 'concrete', index: 5},
	{title: 'blocks', index: 3.5},
];

const floorPrices = [
	{min: 0, max: 5, index: 15},
	{min: 6, max: 9, index: 9},
	{min: 10, max: 15, index: 16},
	{min: 16, max: 30, index: 20},
];

function Building({
	                  floorsNumbers = 5,
	                  address = null,
	                  developer = null,
	                  initialPrice = 1000,
	                  parking = false,
	                  heatingType = null,
	                  buildingType = null,
	                  material = null,
	                  yearOfRelease = null
                  }) {
	this.floorsNumbers = floorsNumbers;
	this.address = address;
	this.developer = developer;
	this.initialPrice = initialPrice;
	this.parking = parking;
	this.heatingType = heatingType;
	this.buildingType = buildingType;
	this.material = material;
	this.yearOfRelease = yearOfRelease;
}

Building.prototype.calcPrice = function (floor) {

	if (!this.address) {
		throw new Error('No address');
	}
	if (!this.material) {
		throw new Error('No material!!!!');
	}
	if (floor > this.floorsNumbers) {
		throw new Error('AAAAAAAAa');
	}

	const material = materialPrices.find(function (material) {
		return material.title === this.material;
	}.bind(this)) || {index: 1};

	const address = destinationPrices.find(function (address) {
		return address.dest === this.address;
	}.bind(this)) || {index: 1};

	const floorPrice = floorPrices.find(function (floorItem) {
		return floor >= floorItem.min && floor <= floorItem.max
	}) || {index: 1};

	return this.initialPrice * (material.index + address.index + floorPrice.index)
};

const b1 = new Building({floorsNumbers: 20, address: 'Obolon', material: 'brick'});
const b2 = new Building({floorsNumbers: 10, address: 'Pechersk', material: 'blocks'});

// b1.calcPrice();