// const json = {
// 	name__server: 'Sergey',
// 	saldo: 100000000000,
// 	address: {
// 		city: 'kyiv',
// 		zip: '02152',
// 		country: 'Ukraine',
// 	},
// 	method: function () {
// return 'value';
// 	}
// };
//
// const {name__server:name,
// 	some_property:magic='default value', /*переименовать и присовить значение, если его нет*/
// 	saldo:saldoNewName,
// 	method:someFunction,
// 	address: {country}
// } = json;
//---------------------------------------------------//
// const jsonServer = {
// 	firstname: 'Max',
// 	lastname: 'Kulibin',
// 	saldo: 1001424,
// 	address: {
// 	city: 'Kyiv',
// 	}
// };
//
// function getName(firstname, lastname) {
// 	return `${firstname} ${lastname}`;
// }
//
// function getNameM1({firstname = 'default name', lastname = 'default family name'}) {
// 	return `${firstname} ${lastname}`;
// }
//
// console.log(getNameM1(jsonServer));

// ------------------------------------------------------------------------
// const jsonFromServer = {
// 	number: '00000012414',
// 	prefix: 'ALPG',
// 	date: '07 OCT 2020',
// 	name: 'Alex',
// 	saldo: 12412435235235
// }
//
// function magic ({prefix = 'empty prefix', number = 'empty number'}) {
// 	return `${prefix}-${number}`;
// }
//
// console.log(magic(jsonFromServer), makeIndexing(0.7, jsonFromServer));
//
// function makeIndexing(index = 1, {saldo = 0}) {
// 	return saldo * index;
// }
// -------------------------------------------------------------------------
//
// const {saldo} = {saldo: 10000000, name: 'Alex'};
//
// const [first,, third, ...rest] = [1,2,3,4,5,6,32,25,23,52];
// console.log(rest);
//
// function useState(initValue = 0) {
// 	let value = initValue;
//
// 	const changeVal = function (v) {
// 		value = v;
// 	};
//
// 	return [value, changeVal];
// }
//
// const [val, setVal] = useState(100);
//
// console.log('val ->', val);
//
// setVal(10000);
//
// console.log('val ->', val);
//
// --------------------------------------------------------
//
// const obj = {
// 	prefix: 'DAN',
// 	number: 21,
// 	size: 20,
// 	name: 'Sergey',
// 	date: new Date(),
// 	comment: ''
// }
//
// function someFunc ({prefix = 'FRE', number = 25, size = 10, ...rest}) {
// 	let startDate = Date.now();
// 	let zeroNumber = size - String(number).length;
// 	const arr = [];
//
// 	for (let i = 0; i < zeroNumber; i++){
// 		arr.push('0');
// 	}
//
// 	let finalNumber = `${prefix}-${arr.join('')}${number}`;
// 	let finalDate = Date.now();
// 	return [finalNumber, finalDate-startDate, rest]
// }
// const [invoiceNumber, performance, rest] = someFunc(obj);
// console.log(invoiceNumber);
// console.log(performance);
// console.log(rest);
// ------------------------------------------------------

// const obj1 = {name: 'Sergey'};
// const obj2 = {saldo: 124124121};
//
// const resultE5 = Object.assign({}, obj1, obj2);
// console.log(resultE5);
// console.log({...obj1, ...obj2});
//
// function doCalc(param1, param2) {
// 	param1.addNewProperty = 10;
// 	param2.addNewProperty = 1121210;
// }
//
// doCalc(obj1, {...obj2}); /*клонирование обьекта*/
// -----------------------------

// const arr1 = [1,2,4,4,2];
// const arr2 = [1414,124,141,212,412,4124,12,421];
// const res = [...arr1, ...arr2];
// console.log(res);
//
// for (const [key, val] of document.getElementsByTagName('div')){
//
// }

