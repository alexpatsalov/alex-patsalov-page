const p1 = {
	name: 'p1',
	doSomethingP1: function (param1, param2) {
		console.log(`Method in P1 --->> ${this.name}`);
		console.log(param1, param2);
		return true;
	}
};

const p2 = {
	name: 'p2',
	doSomethingP2: function () {
		console.log(`Method in P2 --->> ${this.name}`)
	}
};

const newFunction = p1.doSomethingP1.bind(p2);
newFunction('param1', 'param2');