function Person({name, age, height}){
	this.person_name = name;
	this.person_age = age;
	this.person_height = height;
	this.toString = function () {
		return `${this.person_name} | ${this.person_age}`
	};
	this.callPersonMethod = function () {
		return `Calc for person -->> ${this.person_name}`
	}
}

const p = new Person({
	name: 'Vasyl',
	age: 54,
	height: 165
})

const p1 = {
	person_name: 'Max',
	person_age: 24,
	person_height: 182,
	toString: function() {
		return `${this.person_name} ->> ${this.person_age}`
	},
	doHust : function () {
		return `Do Hust ->> ${this.person_name}`
	}
};
// alert(p1);
// alert(p);

console.log(p.callPersonMethod.call(p1));
console.log(p.toString.call(p1));