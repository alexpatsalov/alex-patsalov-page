function bind(context, fn) {
	return function (args) {
		fn.apply(context, args)
	}
}

function Object1 (name) {
	this.name = name;
	this.doSmth1 = function (p1, p2) {
		console.log(`obj1 -->> ${this.name}, ${p1}, ${p2}`)
	}
}

function Object2(name) {
	this.name = name;
	this.doSmth2 = function (p1, p2) {
		console.log(`obj2 -->> ${this.name}.${p1}.${p2}`)
	}
}

let obj1 = new Object1('1');
let obj2 = new Object2('2');

bind(obj1, obj2.doSmth2)('a', 'b');
bind(obj2, obj1.doSmth1)('c', 'd');