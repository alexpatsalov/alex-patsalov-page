const methodName = '';

const p1 = {
	name: 'p1',
	doSomethingP1: function () {
		console.log(`Method in P1 --->> ${this.name}`)
	}
};

const p2 = {
	name: 'p2',
	doSomethingP2: function (param1) {
		console.log(`Method in P2 --->> ${this.name}, ${param1}`)
	}
}

p1.doSomethingP1.call(p2);
p2.doSomethingP2.call(p1);

p1[methodName].call(p2);