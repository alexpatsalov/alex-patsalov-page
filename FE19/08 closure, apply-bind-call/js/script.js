function Person({name, age, height}){
	this.person_name = name;
	this.person_age = age;
	this.person_height = height;
}

// const p = new Person({
// 	name: 'Alex',
// 	age: 26,
// 	height: 182
// });
// console.log(p);

const arr = [
	new Person({
		name: 'Sergey',
		age: 21,
		height: 176
	}),
	new Person({
		name: 'Alex',
		age: 26,
		height: 182
	}),
	new Person({
		name: 'Max',
		age: 18,
		height: 125
	})
];

arr.forEach(person => {console.log(`${person.person_name}`)});