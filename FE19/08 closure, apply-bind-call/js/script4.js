const p1 = {
	name: 'p1',
	doSomethingP1: function (param1, param2) {
		console.log(`Method in P1 --->> ${this.name}`);
		console.log(param1, param2);
		return true;
	}
};

const p2 = {
	name: 'p2',
	doSomethingP2: function () {
		console.log(`Method in P2 --->> ${this.name}`)
	}
}

p1.doSomethingP1.apply(p2, [1231,13,1231]);
// Method in P1 --->> p2
// 1231 13
// true
p1.doSomethingP1.call(p2, 1231,13,1231);
// Method in P1 --->> p2
// 1231 13
// true