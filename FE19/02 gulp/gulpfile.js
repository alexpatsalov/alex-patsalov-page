const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat');
const htmlmin = require('gulp-htmlmin');
const minify = require('gulp-minify');

gulp.task('script:vendor', function () {
	return gulp
		.src([
				'./node_modules/jquery/dist/jquery.js',
				'./node_modules/@fortawesome/fontawesome-free/js/all.js'
			]
		)
		.pipe(uglify())
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest('dist/js'));
});
gulp.task('default', function(){
	// place code for your default task here
});
gulp.task('minify', () => {
	return gulp.src('src/**/*.html')
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest('dist'));
});
gulp.task('script', function () {
	return gulp.src('src/**/*.js')
		.pipe(uglify())
		// .pipe(minify())
		.pipe(gulp.dest('dist'));
});