function getData() {
	return new Promise(((resolve, reject) => {
		try {
			setTimeout(()=>{
				resolve({title: 'data'})
			}, 1500)
		}
		catch (e) {
		reject(e)
		}
	}))
}
//
// getData()
// .then(result =>{
// 	console.log('Result', result);
// })


// async function asyncFunc () {
// 	const data = await getData();
// 	console.log('result', data);
//
// 	return 'Value'
// }
// asyncFunc()
// 	.then((res)=>{
// 		console.log('Async function result',res);
// 	});

