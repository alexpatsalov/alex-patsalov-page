class Task {
	constructor(title, done = false) {
		this.title = title;
		this._done = done;
		this._estimation = 0;
	}

	get done(){
		return this._done ? 'Done' : 'Not done'
	}
	set done(value){
		this._done = value;
	}
	set estimation (value){
		if (value < 0 || value > 12){
			throw new Error('Error in estimation');
		}

		this._estimation = value;
	}
}

const o = new Task ('Hometask');

for (const index in o){
	console.log(`${index} -->> ${o[index]}`);
}