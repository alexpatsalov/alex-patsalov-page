class Group {
	constructor(title, description, items = []) {
		this.title = title;
		this.description = description;
		this._items = items;

	}
	addItem (item){
		this._items.push(item)
		return this;
	}
	toString (){
		return `${this.title}, ${this.description} -->> (${this._items.length})`
	}
	/**
	 * @desc deleteItem
	 * @param {Number} id
	 * @return {boolean}
	 * */
	deleteItemById (id) {
		const index = this._items.findIndex((i) => {
			return i.id === id
		});
		if (index >= 0) {
			this._items.splice(index, 1);
			return true;
		}
		return false;
	}
	get subtitle (){
		return `${this.title} (${this._items.length})`
	}

}

class Item {
	constructor(title) {
		this._id = ++Item.ID_INCREASER;
		this.title = title;
		this.created_at = new Date();
		this.updated_at = new Date();
	}
	get ID (){
		return this._id;
	}

}

Item.ID_INCREASER = 0;

class Product extends Item{
	/**
	 * @param {object} options
	 *  @property {string} title
	 *  @property {number} price
	 * */
	constructor(options) {
		const {title, price} = options;
		super(title);
		this.price = price;

	}

	get price (){
		return `${this._price.toFixed(2)} UAH`
	}
	set price (value){
		if (value < 0){
			throw new Error('Price < 0')
		}
		this._price = +value;
	}
}

const group1 = new Group('TV', 'black');
group1.addItem(new Product({title: 'LG 400pi', price: 3000}))
	  .addItem(new Product({title: 'Samsung', price: 17430}))
	  .addItem(new Product({title: 'apple TV', price: 14122}));


const group2 = new Group('PC', 'macBook');
group2.addItem(new Product({title: 'Lenovo', price: 12220}))
	  .addItem(new Product({title: 'Apple', price: 13000}))
	  .addItem(new Product({title: 'HP', price: 8000}));

alert(group1);
alert(group2);



