function TaskFunc(title) {
	BaseFuncTask.call(this, title);
}

function BaseFuncTask (title){
	this.title = title;
	this.done = false;
}

BaseFuncTask.prototype.complete = function () {
	this.done = true;
}

TaskFunc.prototype = Object.create(BaseFuncTask.prototype);

class BaseClass {
	constructor(title){
		this.title = title;
		this.done = false;
	}
	complete(){
		this.done = true;
	}
}

class Task extends BaseClass{
	constructor(title) {
		super (title);

	}
	complete() {
		this.done = true;
	}
}
const obj1 = new TaskFunc('title1');
const obj2 = new Task('title2');