const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');

sass.compiler = require('node-sass');

gulp.task('t-sass', function () {
	return gulp.src('src/scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('dist/css'));
});
gulp.task('magic', function () {
	return gulp
		.src(
				'./node_modules/bootstrap/dist/css/bootstrap.css',
		)
		.pipe(concat('vendor.css'))
		.pipe(gulp.dest('dist/css'));
});
gulp.task('htmlmin', () => {
	return gulp.src('src/index.html')
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest('dist'));
});