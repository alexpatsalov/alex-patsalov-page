class DomProvider {
	constructor(el = null) {
		this._element = el && this.createElement(el) || null;
	}

	createElement(el) {
		return document.createElement(el);
	}
	get element (){
		return this._element;
	}
	getByID (id){
		return document.getElementById(id);
	}

}

class Table extends DomProvider {
	constructor(col = 10, row = 10) {

		super('table');
		this.col = col;
		this.row = row;
	}

	build() {
		// this.element
		for (let i = 0; i < this.row; i++){
			const row = this.addRow();

			for (let j = 0; j < this.col; j++){
				row.addCell();
			}

			this.element.appendChild(row.element);
		}
		return this.element;
	}
	addRow(){
		return new Row();
	}
}

class TableGame extends DomProvider{
	constructor(el) {
		super();
		this._hostElement = this.getByID(el);
	}
	get wrapper (){
		return this._hostElement;
	}
	start() {
		const table = new Table(3, 3);
		// table.build();
		this.wrapper.appendChild((table.build()));

	}
}

const game = new TableGame('root');
game.start();





class Row extends DomProvider{
	constructor() {
		super('tr');
	}
	addCell (){
		const cell = new Cell();
		this.element.appendChild(cell.element);
		return cell;
	}
}

class Cell extends DomProvider {
	constructor() {
		super('td');
		this.element.innerHTML = 'Hello';
	}

}