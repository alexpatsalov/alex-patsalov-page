const p = new Promise(function(resolve, reject){

	setTimeout(()=>{

		const randNumber = Math.random() * 100;

		if(randNumber < 50){
			reject(new Error('Error happened!!'));
		} else {
			resolve({
				guessNumber: randNumber
			});
		}


	}, 2500)
});

// p.then
// (function () {
//
// }, function (error) {
// 	console.log('ERROR')
// });

p
	.then(function (num) {
		console.log('number', num);
		return new Promise (function(resolve,reject){
			resolve(num.guessNumber + Math.random() * 1000);
		});
	})
	.then(function (val) {
		console.log(val);
	})
	.catch( e =>{
	console.error('CATCH');
	return e
})
.then(e2 =>{
	console.log(e2);
})