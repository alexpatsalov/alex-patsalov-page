class Loader {
	constructor() {
		this._status = null;
		this.images = ['https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png',
			'https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png',
			'https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png',
			'https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png',
			'https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png',
			'https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png']
	}
	load(url) {
		return new Promise((resolve, reject) => {
			let img = document.createElement('img');
			img.src = url;
			img.onload = function () {
				resolve(img);
			}
			img.onerror = function (e) {
				reject(e)
			}
		});
	}
	loadAll(){
		const imgArr = [];
		this.images.forEach(function(src){
			imgArr.push(this.load(src))
		}.bind(this));
		return Promise.all(imgArr);
	}
}

const loader = new Loader();
loader.loadAll()
	.then((imgs)=>{
		console.log(imgs);
		for (let el of imgs){
			document.body.appendChild(el);
		}
	})
// const loader = new Loader()
// loader.load('https://www.talkwalker.com/uploads/2017/00001/mc1/Image%20recognition%20example.png')
// 	.then((el) => {
// 		document.body.appendChild(el);
// 	})
// 	.catch((e) => {
// 		console.log('Error--->', e)
// 	});