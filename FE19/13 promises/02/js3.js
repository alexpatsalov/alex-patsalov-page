function first() {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve([{title: 'First article', viewers: 1234}])
		}, 2000)
	});
}

function second() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			reject([{title: 'Second article', viewers: 2222}])
		}, 2000)
	});
}

Promise
	.all([first(), second()])
	.then(function (val) {
		console.log('success', val);
	})
.catch(e =>{
	console.log('error',e);
})