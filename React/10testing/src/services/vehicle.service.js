import {request} from "./core";
import VehicleModel from "./models/vehicle.model";

export default class VehicleService{
    static async getVehicles(){
        const {data} = request({url: "/vehicles"});
        console.log('vehicles -->',data);

        return data.map(item => new VehicleModel(item))
    }
}