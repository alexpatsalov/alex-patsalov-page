class localStorageService{
    constructor() {
        this.key = 'app key'
    }
    setItem(value){
        return localStorage.setItem(this.key, JSON.stringify(value))
    }
    getItem(){
        try {
            return JSON.parse(localStorage.getItem(this.key))
        } catch (e){
            console.log('error-->', e)
            return {}
        }

        // return JSON.parse(localStorage.getItem(this.key))
    }
}
export default new localStorageService;