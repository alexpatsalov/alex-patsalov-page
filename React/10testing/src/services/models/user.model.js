export default class User{
    constructor({email, name, password, id, department}) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.id = id;
        this.department = department;
    }
}