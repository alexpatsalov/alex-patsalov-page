import MainRoute from "../containers/route";
import {Menu} from 'antd';
// import {MailOutlined} from '@ant-design/icons';
import './App.css';
import {Link} from "react-router-dom";
import {useState} from "react";
import {useSelector} from 'react-redux';


function App() {
    const [current, setCurrent] = useState();
    const auth = useSelector((state)=>{
        return state.isAuth
    });
    const handleClick = e => {
        console.log(e.key);
        setCurrent(e.key);
    }
    return (
        <>
            <header>
                <Menu onClick={handleClick} selectedKeys={current} mode="horizontal">
                    <Menu.Item key="home">
                        <Link to="/">Home</Link>
                    </Menu.Item>

                    <Menu.Item key="login" disabled={auth}>
                        <Link to="/login">Login</Link>
                    </Menu.Item>

                    <Menu.Item key="vehicles" disabled={!auth}>
                        <Link to="/vehicle">Vehicles</Link>
                    </Menu.Item>
                </Menu>
            </header>
            <MainRoute/>
        </>
    );
}
export default App;