import {Switch, Route} from "react-router-dom";
import HomePage from "../../Components/pages/Home";
import AuthorizePage from "../../Components/pages/Authorize";
import VehiclePage from "../../Components/pages/Vehicle";

export default function MainRoute() {
    return (
        <main>
            <Switch>
                <Route exact path='/home' component={HomePage}/>
                <Route exact path='/vehicle' component={VehiclePage}/>
                <Route exact path='/login' component={AuthorizePage}/>
            </Switch>
        </main>
    );
}