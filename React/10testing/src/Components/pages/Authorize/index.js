import {Form, Input, Button, Checkbox} from 'antd';
import './styles.css';
import {useDispatch, useSelector} from "react-redux";
import {loginAction} from "../../../redux/actions/authAction";
import {useState} from "react";
import {useHistory, Redirect} from 'react-router-dom';
import UserService from "../../../services/user.service";
import StorageInstance from '../../../services/localStorage.service'

export default function AuthorizePage() {

    const [logining, setLogining] = useState(false);
    const history = useHistory();
    const auth = useSelector((state) => {
        return state.isAuth
    });
    const loginDispatch = useDispatch();

    const onFinish = async (values) => {
        try {
            setLogining(true);
            const user = await UserService.authorize(values);
            // loginDispatch(loginAction(values));
            console.log('user-->', user);
            StorageInstance.setItem(user);
            loginDispatch(loginAction(values));
            history.push('/home');
        } catch (e) {
            console.log('error-->>', e);

        } finally {
            setLogining(false);

        }
    }

    return (
        <>
            <h1>authorize page</h1>
            <div className="login-page">
                {
                    auth ?
                        <Redirect to={'/home'}>

                        </Redirect>
                        :
                        <Form onFinish={onFinish} initialValues={{remember: true}}>
                            <Form.Item
                                hasFeedback={true}
                                help={'help text'}
                                tooltip={'tooltip text'}
                                label="Email"
                                name="email"
                                rules={[{required: true, type: 'email', message: 'Please input your username!'}]}
                            >
                                <Input placeholder="input email"/>
                            </Form.Item>
                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{required: true, message: 'Please input your password!'}]}
                            >
                                <Input.Password placeholder="input password"/>
                            </Form.Item>
                            <Form.Item name="remember" valuePropName="checked">
                                <Checkbox>Remember me</Checkbox>
                            </Form.Item>
                            <Form.Item>
                                <Button disabled={logining} type="primary" htmlType="submit">
                                    Submit
                                </Button>
                            </Form.Item>
                        </Form>
                }

            </div>
        </>
    );
}