import VehicleService from "../../../services/vehicle.service";
import {useEffect, useState} from 'react';

export default function VehiclePage() {

    const [vehicles, setVehicles] = useState([]);
    useEffect(async ()=>{
        const res = await VehicleService.getVehicles();
        // setVehicles(vehicles.name);
        console.log(res);
    }, [])

    return (
        <>
            <h1>vehicle page</h1>
            <div>
                {vehicles.map (item => <li key={item.id}>{item.name}</li>)}
                {/*{VehicleService.getVehicles()}*/}
            </div>
        </>
    );


}