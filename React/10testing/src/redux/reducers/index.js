import {ACTION_LOGIN} from "../actions/authAction";
import LocalStorageService from '../../services/localStorage.service';

const storage = LocalStorageService.getItem();

const initialState = {
    isAuthorized: !!(storage && storage.token),
}

export function appReducer(state = initialState, action){
    switch (action.type){

        case ACTION_LOGIN:
            return {
                ...state,
                isAuth:true
            }
        default: return state
    }
}

