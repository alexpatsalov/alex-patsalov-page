import React from 'react';

export default function PeopleList({items, selected}) {
    const style = {
        backgroundColor: selected ? 'red' : 'blue'
    }
    return (<ul style={style}>{items.map(item => <li key={item.name}>{item.name}</li>)}</ul>);
}