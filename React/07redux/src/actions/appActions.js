export const SET_CURRENT_FIGURE = 'app/SET_CURRENT_FIGURE';

export function setCurrentFigure(figure){
    return{
        type: SET_CURRENT_FIGURE,
        payload: figure
    }
}