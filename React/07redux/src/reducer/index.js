import {triangleReducer} from './triangle';
import {rectangleReducer} from './rectangle';
import {squareReducer} from './square';
import {combineReducers} from "redux";
import {appReducer} from "./app";

export const rootReducer = combineReducers({
    triangle: triangleReducer,
    square: squareReducer,
    rectangle: rectangleReducer,
    app: appReducer
})