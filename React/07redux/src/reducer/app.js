import {TYPE_RECTANGLE, TYPE_SQUARE, TYPE_TRIANGLE} from "../components/Figures";
import {SET_CURRENT_FIGURE} from "../actions/appActions";

const initialState = {
    currentFigure: TYPE_TRIANGLE,
    figures: [
        {
            title: 'трикутник',
            type: TYPE_TRIANGLE
        },
        {
            title: 'прямокутник',
            type: TYPE_RECTANGLE
        },
        {
            title: 'квадрат',
            type: TYPE_SQUARE
        }
    ]
}
export function appReducer(state = initialState, action){
    switch (action.type){
        case SET_CURRENT_FIGURE:
            return {
                ...state,
                currentFigure: action.payload
            }

        default: return state
    }
}