import React from 'react';
// import PropTypes from 'prop-types';
import FigureSelect from "../FigureSelect";
import CalculationResults from "../CalculationResult";
import './style.css';
import {TYPE_RECTANGLE, TYPE_SQUARE, TYPE_TRIANGLE} from "../Figures";
import {connect} from 'react-redux';
import {setCurrentFigure} from "../../actions/appActions";


class Toolbar extends React.Component {

    onSelectorChange = (value) =>{
        console.log(value);
        this.props.setCurrentFigure(value);
    }

    render() {
        return (

            <div className='toolbar-wrapper'>
                <FigureSelect onChange={this.onSelectorChange}
                              figures={this.props.figures}/>
                <CalculationResults result={100}/>
            </div>
        );
    }
}

const mapStateToProps = store =>{
    return{
        figures: store.app.figures
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        setCurrentFigure: figure => dispatch(setCurrentFigure(figure))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);