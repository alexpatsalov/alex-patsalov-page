import Triangle from './Triangle';
import Square from './Square';
import Rectangle from './Rectangle';

const TYPE_TRIANGLE = 'TRIANGLE';
const TYPE_SQUARE = 'SQUARE';
const TYPE_RECTANGLE = 'RECTANGLE';

export {Triangle, Square, Rectangle, TYPE_RECTANGLE, TYPE_SQUARE, TYPE_TRIANGLE};