import React from 'react';
import PropTypes from 'prop-types';


class FigureSelect extends React.Component {
    constructor(props) {
        super(props);
    }

    onChange = (e)=>{
        this.props.onChange(e.target.value)
    }
    render() {
        const {figures} = this.props;
        return (
            <div>
                <select name="select" id="select" onChange={this.onChange}>
                    {figures.map (item =><option value={item.type}
                                                 key={item.type}>
                        {item.title}</option>)}
                </select>
            </div>
        );
    }
}
export default FigureSelect;

// FigureSelect.propTypes = {
//     figures: PropTypes.array.isRequired
// }