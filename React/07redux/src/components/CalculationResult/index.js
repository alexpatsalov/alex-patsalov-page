import React from 'react';
import PropTypes from 'prop-types';

class CalculationResults extends React.Component {
    render() {
        return (
            <div className='res-wrapper'>
                <p>Result -->> {this.props.result}</p>
            </div>
        );
    }
}
export default CalculationResults;

// CalculationResults.propTypes = {
//     result: PropTypes.number.isRequired
// }