import React from 'react';
// import PropTypes from 'prop-types';
import {TYPE_RECTANGLE, TYPE_SQUARE, TYPE_TRIANGLE, Rectangle, Square, Triangle} from "../Figures";
import {connect} from 'react-redux';

class Main extends React.Component {

    render() {
        return (
            <div className='main'>
                {
                    this.props.figure === TYPE_RECTANGLE
                        ? <Rectangle/>
                        : this.props.figure === TYPE_SQUARE
                        ? <Square/>
                        : <Triangle/>
                }
            </div>
        );
    }
}

const mapStateToProps = store =>{
    return {
        figure: store.app.currentFigure
    }
}

export default connect(mapStateToProps)(Main);