import React from 'react';
import UserCard from "../UserCard";
import users from '../../data/users.json';
import {UserService} from "../../services/userService"

export default function UserList(){
    const users = UserService.loadUsers();
    console.log(users);
    return (
        <div>
            <div className="user-list card-group">
                {users
                    .map
                    (user =>
                        <UserCard user={user} key={user.id}/>
                    )}
            </div>
        </div>
    );
}