import React from 'react';
import {userModel} from '../../services/models'

export default function UserCard(props){
    // console.log(props);
    const {user} = props;
    return (
        <div className="card">
            <div className="card-body">
                <h5 className="card-title">
                    {user.name}
                </h5>
                <p className="card-text">
                    {`${user.city}, ${user.birthday}`}
                </p>
            </div>
            <div className="card-footer">
                <small className="text-muted">
                    {`${user.department}`}
                </small>
            </div>
        </div>

    );
}