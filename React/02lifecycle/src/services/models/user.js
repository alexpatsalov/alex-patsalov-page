import moment from 'moment';

export default class User {
    constructor({first_name, last_name, middle_name, birthday, department, city, id}) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.middle_name = middle_name;
        this._birthday = birthday;
        this.department = department;
        this.city = city;
        this.id = id;
    }

    get name (){
        return `${this.first_name} ${this.middle_name} ${this.last_name}`
    }
    get birthday (){
        return this._birthday ? moment(this._birthday).format('DD.MM.YYYY') : 'N/A'
    }
}