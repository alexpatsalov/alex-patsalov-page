import users from '../data/users.json';
import {userModel} from "./models";

export class UserService {
    constructor() {
    }
    static loadUsers(){
        return users.map(user => new userModel(user))
    }
}