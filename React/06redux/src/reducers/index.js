import {combineReducers} from "redux";
import {userReducer} from "./user";
import {pageReducer} from "./page";


export const rootReducer = combineReducers({
    page: pageReducer,
    user: userReducer
})

// export const initialState = {
//     user: {
//         name: 'Max',
//         surname: 'Maximov',
//         age: 15
//     },
//     position: 0,
//     rate: 0,
//     balance: 0
// };
//
// export function rootReducer(state = initialState){
//     return state
// }