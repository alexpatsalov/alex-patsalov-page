import {SET_YEAR} from "../action/PageActions";

const initialState = {
    year: 2018,
    photos: []
}

export function pageReducer(state = initialState, action) {
    switch (action.type) {
        case SET_YEAR:
            return {
                ...state,
                year: action.payload
            };
        case 'RANDOM_YEAR':
            return {
                ...state,
                year: Math.ceil(Math.random()* 5000)
            }
        default:
            return state
    }
}