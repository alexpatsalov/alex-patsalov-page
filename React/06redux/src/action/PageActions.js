export const SET_YEAR = 'page/SET_YEAR';

export function setYear(year){
    return{
        type: SET_YEAR,
        payload: year
    }
}