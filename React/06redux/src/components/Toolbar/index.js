import React from 'react';
import {connect} from 'react-redux'

class Toolbar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {year} = this.props.page;
        return (
            <div>
                Toolbar -->> {year}
            </div>
        );
    }
}

const mapStateToStore = store => ({
    page: store.page
})

export default connect(mapStateToStore)(Toolbar)