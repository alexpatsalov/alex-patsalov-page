import React from 'react';
import PropTypes from 'prop-types';

export class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    onBtnClick = e =>{
        const year = +e.currentTarget.innerText;
        this.props.setYear(year);
    }

    render() {

        const {photos, year} = this.props;
        return (
            <>
                <div>
                    You have {photos.length} photos in {year}.
                </div>
                <div>
                    <button onClick={this.onBtnClick}>2016</button>
                    <button onClick={this.onBtnClick}>2017</button>
                    <button onClick={this.onBtnClick}>2018</button>
                    <button onClick={this.onBtnClick}>2019</button>
                    <button onClick={this.onBtnClick}>2020</button>
                </div>
            </>
        );
    }
}

Page.propTypes = {
    photos: PropTypes.array.isRequired,
    year: PropTypes.number.isRequired
}