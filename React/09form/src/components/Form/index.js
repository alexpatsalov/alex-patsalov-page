// import React from 'react';
import {useState} from 'react';

export default function Form({onSubmit}){

    const [data, setData] = useState({});

    const onFormSubmit = (e) => {
        e.preventDefault();
        onSubmit(data);
    }

    const onElemChange = ({target: {name, value}}) =>{
        setData(
            {
                ...data,
                ...{[name]: value}
            }
        )
    }

    return(
        <div>
            <form onSubmit={onFormSubmit}>
                <div className='form-item'>
                    <label htmlFor='elementEmail'>Email:</label>
                    <input type="text"
                           name='email'
                           id='elementEmail'
                           value={data.email} onChange={onElemChange}/>
                </div>
                <div className='form-item'>
                    <label htmlFor='elementName'>Name:</label>
                    <input type="text"
                           minLength='5'
                           name='name'
                           id='elementName'
                           value={data.name} onChange={onElemChange}/>
                </div>
                <div>
                    <button type='submit'>Send form</button>
                </div>
            </form>
        </div>
    );
}