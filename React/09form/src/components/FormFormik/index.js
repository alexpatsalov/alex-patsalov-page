import React from 'react';
import {Formik, Form, Field} from 'formik';
import * as Yup from 'yup';

const subscriptionSchema = Yup.object().shape({
    name: Yup.string()
        .required('Required')
        .min(6, 'Name must have at least 6 chars'),

    email: Yup.string()
        .email('Enter valid email')
        .required('Email is required')
})

export default function FormFormik({onSubmit}){

    const onSubmitForm = (data) =>{
        onSubmit(data);
    }

    return(
        <div className='formik-form'>
            <Formik
                initialValues={{
                    name: 'default name',
                    email: 'email on default',
                }}
                validationSchema={subscriptionSchema}
                onSubmit={onSubmitForm}>

            </Formik>
        </div>
    );
}