import React from 'react';
import './style.css'

export default class Counter extends React.Component {
    constructor(props) {
        console.log('constructor');
        super(props);
        this.state = {
            loading: true,
            counter: 0
        };

        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick() {
        console.log('hello', this);
        setTimeout(() => {
            this.setState({
                loading: true,
                counter: this.state.counter + 1
            })
        }, 1000)
    }

    componentDidMount() {
        console.log('didmount called');
        setTimeout(() => {
            this.setState({
                loading: false
            })
        }, 1000)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('updated');


    }

    render() {
        console.log('render happens');
        let Loading = <div>Loading...</div>
        let Stat = <>
            <div className='counter-stat'>{this.state.counter}</div>
            <button onClick={this.onBtnClick}>CLick me</button>
        </>;
        return (
            <div className='counter-wrapper'>
                {this.state.loading ? (
                    Loading
                ) : (
                    Stat
                )}
            </div>
        );
    }
}