import React from 'react';
import './Menu.css';
import MenuItem from "./Item";

export default function Menu(props) {
    const {style = 'light'} = props;
    const items = [
        {url: '0', title: 'Menu item', style},
        {url: '1', title: 'Menu item1'},
        {url: '2', title: 'Menu item2'}
    ];

    return (
        <div className='menu-wrapper'>
            <ul>
                {items.map(item => <MenuItem key={item.url}
                                              url={item.url}
                                              title={item.title}/>
                )}
            </ul>
        </div>
    )
}