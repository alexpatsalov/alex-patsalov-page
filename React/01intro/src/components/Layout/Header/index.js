import React from 'react';
import Menu from "../../common/Menu";

export default function Header() {
    return (
        <div className='header'>
            <section className='logo'>this is logo</section>
            <section className='menu'>
                <Menu/>
            </section>
            <section>
                User menu
            </section>
            This is header
        </div>

    );
}