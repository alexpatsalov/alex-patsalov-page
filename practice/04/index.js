// let one = [true, false, false, true, false];
// let two = [false, false, true, false, false];
// let three = [];
// function count (array){
//     let counter = 0;
//     if(!array){
//         return 0
//     }
//     for (let i = 0; i < array.length; i++){
//         if (array[i] === true){
//             counter++
//         }
//     }
//     return counter
// }
// console.log(count(one)); //2
// console.log(count(two));// 1
// console.log(count(three));//0


let one = '0010110';
let two = '1100000';
let three = '1111111';

function spliting(str) {
    let counter = 0;
    let arr;
    let array;
    let splitted = str.split('');
    splitted.forEach(item => {
        if (item === '1') {
            counter++;
        }
    })
    if (counter % 2 === 0) {
        arr = splitted.join('');
        array = [...arr, '0'];
    } else {
        arr = splitted.join('');
        array = [...arr, '1'];
    }
    return array.join('').toString();
}

console.log(spliting('0010110'));//1
console.log(spliting('0011110'));//0
console.log(spliting('1111111'));//1