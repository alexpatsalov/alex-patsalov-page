export const appAction = (name) => dispatch => {
    dispatch({
        type: 'CHANGE_NAME',
        payload: name
    })
}