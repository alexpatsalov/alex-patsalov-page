const initialState = {
    name: ''
}

export function appReducer (state = initialState, action){
    switch(action.type){
        case 'CHANGE_NAME':
            return {
                ...state,
                name: action.payload
            }
        default:
            return state
    }

}