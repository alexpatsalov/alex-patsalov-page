import logo from './logo.svg';
import './App.css';
import {Switch, Route} from "react-router-dom";
// import Route from "react-router-dom";

function App() {
    return (
        <Switch>
            <Route exact path={'/'} component={Home}/>
            <Route exact path={'/register'} component={Register}/>
            <Route exact path={'/login'} component={Login}/>
            <Route exact path={'/cart'} component={Cart}/>
            <Route exact path={'/error'} component={ErrorComponent}/>
            <Route exact path={'/shop'} component={ProductList}/>
            <Route exact path={'/product-details'} component={ProductDetails}/>
            <Route exact path="*" render={() => <div>Page is not found</div>}/>
        </Switch>
    );
}

export default App;
