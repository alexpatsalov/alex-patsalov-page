// Write a method that returns array of all the numbers from 1 to an integer argument. But for multiples of three use “Fizz” instead of the number and for the multiples of five use “Buzz”. For numbers which are multiples of both three and five use “FizzBuzz”.
//
// Example
// fizzBuzz(10);
// ➞ [1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz"]
//
// fizzBuzz(15);
// ➞ [1, 2, "Fizz", 4, "Buzz", "Fizz", 7, 8, "Fizz", "Buzz", 11, "Fizz", 13, 14, "FizzBuzz"]

// function fizzBuzz(num) {
//     let arr = [];
//     for (let i = 1; i <= num; i++){
//         if(i % 15 === 0){
//             arr.push('FizzBuzz');
//         } else if (i % 5 === 0){
//             arr.push('Buzz');
//         } else if (i % 3 === 0){
//             arr.push('Fizz');
//         } else {
//             arr.push(i);
//         }
//     }
//     return arr
//     console.log(arr);
// }
// function create (num){
//     let arr = [];
//     for (let i = 1; i <= num; i++){
//         arr.push(i);
//     }
//     return arr;
// }

// Create a function that takes a string and returns the letters that occur only once.
//
//     Examples
// findLetters("monopoly") ➞ ["m", "n", "p", "l", "y"]
//
// findLetters("balloon") ➞ ["b", "a", "n"]
//
// findLetters("analysis") ➞ ["n", "l", "y", "i"]
// Notes
// The final array should not include letters that appear more than once in the string.
//     Return the letters in the sequence they were originally in, do not sort them.
//     All letters will be in lowercase.

// function uniqueLetters(str) {
//     console.log ([...str]
//         .filter(x => str.indexOf(x) === str.lastIndexOf(x)))
// }
//
// uniqueLetters('balloon');