// 1 Call

const person = {
    name: "Alex"
}

function info(age, city){
    console.log(`Name ->${this.name}, Age -> ${age}, City -> ${city}`)
}

// function cally (fn, context, ...args) {
//
//     return function () {
//         const uniqueID = Date.now().toString();
//         context[uniqueID] = fn;
//         const res = context[uniqueID](...args);
//
//         delete context[uniqueID]
//         return res
//     }()
// }

// cally(info, person, '26', 'Kyiv');

//2 Apply

// function apply (fn, context, args) {
//
//     return function () {
//         const uniqueID = Date.now().toString();
//         context[uniqueID] = fn;
//         const res = context[uniqueID](...args);
//
//         delete context[uniqueID]
//         return res
//     }()
// }
//
// apply(info, person, ['26', 'Kyiv']);

// 3 Bind

// function bind (fn, context, ...args){
//
//     return function (...rest){
//         return fn.call(context, ...args.concat(rest))
        // return fn.apply(context, args.concat(rest))
    // }
// }

//4 Bind2

// function bind(fn, context, ...rest){
//
//     return function (...args){
//         const uniqID = Date.now().toString();
//         context[uniqID] = fn;
//         const res = context[uniqID](...rest.concat(args));
//
//         delete context[uniqID];
//
//         return res
//     }
// }
//
// bind(info, person, '26', 'Kyiv')();
// bind(info, person, '26')('Kyiv');
// bind(info, person)('26', 'Kyiv');

