export const initialState = {
    data: [
        {
            "name": "Samsung S10",
            "price": "$1000",
            "url": "https://i2.rozetka.ua/goods/11052552/samsung_galaxy_s10_6_128_gb_black_sm_g973fzkdsek_images_11052552435.jpg",
            "article": "21",
            "color": "red",
            "id": "1"
        },
        {
            "name": "Iphone 12",
            "price": "$1200",
            "url": "https://i8.rozetka.ua/goods/20300920/245162197_images_20300920671.jpg",
            "article": "18",
            "color": "white",
            "id": "2"
        },
        {
            "name": "ZTE A3",
            "price": "$650",
            "url": "https://i8.rozetka.ua/goods/17898128/zte_blade_a3_2020_grey_images_17898128749.jpg",
            "article": "4",
            "color": "dark grey",
            "id": "3"
        },
        {
            "name": "Realme 6 PRO",
            "price": "$500",
            "url": "https://i2.rozetka.ua/goods/18002337/copy_realme_6941399013223_5ea7f0ac6ca74_images_18002337787.jpg",
            "article": "7",
            "color": "gold",
            "id": "4"
        },
        {
            "name": "Iphone SE",
            "price": "$850",
            "url": "https://i8.rozetka.ua/goods/17801465/apple_iphone_se_64gb_black_images_17801465989.png",
            "article": "14",
            "color": "black",
            "id": "5"
        },
        {
            "name": "Nokia 8",
            "price": "$400",
            "url": "https://i8.rozetka.ua/goods/20646151/260954841_images_20646151391.jpg",
            "article": "11",
            "color": "cyan",
            "id": "6"
        },
        {
            "name": "Samsung S10",
            "price": "$1000",
            "url": "https://i2.rozetka.ua/goods/11052552/samsung_galaxy_s10_6_128_gb_black_sm_g973fzkdsek_images_11052552435.jpg",
            "article": "41",
            "color": "black",
            "id": "7"
        },
        {
            "name": "Huawei P Smart",
            "price": "$700",
            "url": "https://i2.rozetka.ua/goods/20477890/huawei_p_smart_2021_128gb_gold_images_20477890541.png",
            "article": "6",
            "color": "gold",
            "id": "8"
        },
        {
            "name": "Meizu 16",
            "price": "$1100",
            "url": "https://i1.rozetka.ua/goods/15446836/meizu_16_6_64gb_black_images_15446836385.jpg",
            "article": "27",
            "color": "silver",
            "id": "9"
        },
        {
            "name": "Samsung S20",
            "price": "$1200",
            "url": "https://i8.rozetka.ua/goods/16848210/samsung_galaxy_s20_plus_gray_sm_g985fzadsek_images_16848210807.jpg",
            "article": "13",
            "color": "grey",
            "id": "10"
        }
    ],
    favourites: [],
    cart: [],
}

export function rootReducer(state = initialState) {
    return state
}