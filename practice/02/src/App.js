import React from 'react';
import './App.css'
import getProductList from "./services/productService";
import {useState, useEffect} from 'react';
import Main from "./components/Main";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

export function App(props) {

    const [products, setProducts] = useState([]);
    const [favourites, setFavourites] = useState([]);
    const [cart, setCart] = useState([]);

    const addToCart = (e) => {
        let clonedCart = [...cart];
        let product = JSON.parse(e.target.dataset.cart);
        clonedCart.push(product);
        setCart([...clonedCart]);
        localStorage.setItem('cart', JSON.stringify(clonedCart));
    }

    const addToFavourites = (e) => {
        let clickedStar = e.target;
        let clickedProduct = JSON.parse(e.target.dataset.product);
        let clonedFavourites = [...favourites];
        let index = clonedFavourites.findIndex(item => item.id === clickedProduct.id);
        if (index < 0) {
            clickedStar.classList.toggle('button-star-clicked');
            clonedFavourites.push(clickedProduct);
            localStorage.setItem('favourites', JSON.stringify(clonedFavourites));
            setFavourites([...clonedFavourites]);
        }
    }

    useEffect(function () {
        async function fetchData() {
            const {data} = await getProductList();
            setProducts(data);
        }

        fetchData();
    }, [])

    console.log('---RENDER---');

    // let {data} = props;
    console.log(props);

    return (
        <div className="App">
            <div className="links-container">
                <Link to={`/`}>Home |</Link>
                <Link to={`/favourites/`}>Favourites |</Link>
                <Link to={`/cart/`}>Cart</Link>
            </div>
            <Main cart={cart} setCart={setCart} favourites={favourites} products={products} addToCart={addToCart}
                  addToFavourites={addToFavourites} setFavourites={setFavourites}/>
        </div>
    );
}

const mapStateToProps = (store) =>{
    console.log(store);
    return {
        products: store.products
    }
}

export default connect (mapStateToProps)(App);