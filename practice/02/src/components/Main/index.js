import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from "../Home";
import Favourites from "../Favourites";
import Cart from "../Cart";

export default function Main({favourites, setFavourites, products, cart, setCart, addToFavourites, addToCart}) {

    return (
        <main>
            <Switch>
                <Route exact path='/' render={() =>
                    <Home
                        favourites={favourites}
                        cart={cart}
                        products={products}
                        addToFavourites={addToFavourites}
                        addToCart={addToCart}
                    />}
                />
                <Route path='/favourites' render={() =>
                    <Favourites
                        favourites={favourites}
                        setFavourites={setFavourites}/>}
                />
                <Route path='/cart' render={() =>
                    <Cart cart={cart} setCart={setCart}/>}/>
            </Switch>
        </main>
    );
}